#+title: Adaptive Dynamics -- Tutorial
#+author: Guilhem Doulcier
#+options: toc:nil
#+latex_header: \usepackage{a4wide}

The objective of this tutorial is to teach the student about practical
adaptive dynamics.

*Concepts*: Assumptions of adaptive dynamics, invasion fitness, Pairwise
invasibility plot.

* Biological problem 
/In this section the T.A. presents a (hopefully) interesting biological problem, and ask the students what method could be used to solve it (spoiler: here it's adaptive dynamics). The class then come back on the important results about this method learnt from the course./\\

It has been observed that animals seems to present morphological
characteristics that seems adapted to their foraging
technique. Observing fishes we see deep body shape in the littoral
zone and when preying on sedentary benthic prey, predicted to be
adaptive since it allows a higher precision in maneuverability (Webb
1984; Domenici 2003). In contrast, a shallow body shape is predicted
to be beneficial when foraging on planktonic prey (Webb 1984; Domenici
2003).  

What are the evolutionary consequence of such a partition of
ressource ? Will we see pure strategies evolve (only hunting benthic
prey or foraging plankton) or mixed ? Under which conditions ? Could
it lead to speciation ?

What will we use to solve this ? *Adaptive dynamics* ! (of course this
is the title of the tutorial). 

Reminder of the main hypothesis of adaptive dynamics (Geritz 1992):

We separate ecological and evolutionary timescale by considering that
mutations are rare enough so they appear in a monomorphic resident
mutation. From there they can either invade or disappear. We are just
interested in their ability to invade (measured by invasion
fitness, i.e. the exponential growth rate of a rare mutant). 

Two important assumptions (to be written on the blackboard):
- The resident is at ecological equilibrium. 
- The mutant is at low initial density. 


* Modeling the problem  
/In this section the T.A. build the model on the blackboard interactively with the students. The order in which the assumptions are presented here is indicative only but present a logical progression that can be useful to direct the discussion./

** Population dynamics
Let us start by modeling the population dynamics. Here is the list of
assumptions...

- There is a consumer $N$, and two ressources ($F_1$ and $F_2$)
  occuring in two habitats.
- Population are unstructered, asexual, big enough to be modeled in
  $\mathbb R \to \mathbb{R}^3$, $t \mapsto (N(t), F_1(t), F_2(t))$
  (continuous time, continuous population, no explicit
  spacialization).
- Ressource $1$ (resp $2$) follow a Chemostat dynamics with dilution
  rate $\delta_1$ (resp. $\delta_2$) and carrying capacity $K_1$
  (resp. $K_2$). It has been argued (Persson 1998) that it was a
  realistic model of resources if (1) the resource has a physical
  refuge, (2) the resource has adult class size that are too small to
  be eaten (invulnerable) and then grow into vulnerable class
  size. (Here, doing a rough sketch of $\frac{dN}{dt} = f(N)$ can give a
  good idea of the qualitative differences between the two models).
- Consumers have a per capita birth rate $\beta$ and per capita death
  $\mu$ rate.

We get to:

\begin{equation*}
\begin{cases}
\frac{dF_1}{dt} = \delta_1 (K_1 - F_1(t))  & \text{Density of prey in env. 1} \\
\frac{dF_2}{dt} = \delta_2 (K_2 - F_2(t))  & \text{Density of prey in env. 2} \\ 
\frac{dN}{dt}  =  (\beta - \mu) N(t) & \text{Density of fish}
\end{cases}
\end{equation*}

** Predator-prey interactions
Let us walk through the hypothesis behind the interactions...
- Spacialization is implicit in the interaction terms. No direct
  interaction between $F_1$ and $F_2$. Consumer have a heritable trait
  (denoted by $u$) which is the fraction of their lifetime spend
  foraging on ressource 1.
- Holling Functional response of type 1 (per capita consumption 
  is linear with the prey density). "$- \alpha F_1(t)N(t)$"
- The value of the consumption rate is given by a search-succes rate
  in a given environment multiplied by the time spent in the
  environment: $u A_1$ and $(1-u)A_2$
- Empirical evidence suggest that $A_2: u \mapsto A_2(u) = a_2$ is constant (Andersson
  2003). We assume that $A_1$ is an afine function of $u$: $u \mapsto A_1(u) = a_1+b_1u$. 
- The consumer per capita birth rate is proportional to the
  consumption rate.
 
We get to: 

\begin{equation*}
\begin{cases}
\frac{dF_1}{dt} = \delta_1 (K_1 - F_1(t)) - F_1(t)N(t)uA_1(u)  & \text{Density of prey in env. 1} \\
\frac{dF_2}{dt} = \delta_2 (K_2 - F_2(t)) - F_2(t)N(t)(1-u)A_2(u) & \text{Density of prey in env. 2} \\ 
\frac{dN}{dt}  =  (\beta(u) - \mu) N(t) & \text{Density of fish}
\end{cases}
\end{equation*}

\begin{equation}
\beta(u) = 
\underbrace{F_1(t)}_{\text{(prey density)}}
\underbrace{A_1(u)}_{\text{(search rate)}}
\underbrace{u}_{\text{(time spent)}}
+ 
\underbrace{F_2(t)}_{\text{(prey density)}}
\underbrace{A_2(u)}_{\text{(search rate)}}
\underbrace{(1-u)}_{\text{(time spent)}}
\end{equation}

** Adaptive dynamics framework 

Now we want to apply our Adaptive dynamics method on the heritable
trait $u$. Remember that we need a rare mutant in a stable monomorphic
resident population.

We start by adding an equation to the system to add a mutant type. We
denote $r$ the resident trait and $m$ the mutant trait.

\begin{align}
\begin{cases}
\frac{dN_r}{dt} &= (\beta(r) - \mu) N_r(t)\\
\frac{dN_m}{dt} &= (\beta(m) - \mu) N_m(t)\\
\frac{dF_1}{dt} &= \delta_1 (K_1 - F_1(t)) - F_1(t)[N_r(t)rA_1(r) + N_m(t)mA_1(m)]\\
\frac{dF_2}{dt} &= \delta_2 (K_1 - F_2(t)) - F_2(t)[N_r(t)(1-r)A_2(r) + N_m(t)(1-m)A_2(m)]
\end{cases}
\end{align}

We can simplify this cumbersome system by eliminating the ressource
dynamics with a QSS (Quasy Steady State) approximation (which is
coherent with our biological assumption that ressources follow a
chemostat dynamics):

\begin{align*}
\frac{dF_1}{dt} = \frac{dF_2}{dt} =& 0 \\ 
\Leftrightarrow& \begin{cases}
F_1^*(N_r, N_m) = \frac{\delta_1 K_1}{\delta_1 + N_r(t)rA_1(r) + N_m(t)mA_1(m)} \\
F_2^*(N_r, N_m) = \frac{\delta_2 K_2}{\delta_2 + N_r(t)(1-r)A_2(r) + N_m(t)(1-m)A_2(m)} \\
\end{cases}
\end{align*}

Thus the system becomes:

\begin{align}
\begin{cases}
\frac{dN_r}{dt} &= [\beta(r, N_r(t), N_m(t)) - \mu] N_r(t)\\
\frac{dN_m}{dt} &= [\beta(m, N_r(t), N_m(t)) - \mu] N_m(t)\\
\end{cases}
\end{align}

Our goal is to compute the invasion growth rate of a mutant (with
trait $m$) in an environment set by a resident (with trait value
$r$). We denote this quantity $s_r(m)$.\\

Now we factor in the assumptions of Adaptive Dynamics (/Geritz 1998/): 

1.  **The resident is at ecological equilibrium:** $\frac{dN_r}{dt} = 0; \; N_r(t) = N^*;$
2. **The mutant is at low density:** $N_m \approx 0$

$N^*$ is given by solving $\beta(r, N_r(t), N_m(t)) - \mu = 0$ for
$N(t)$ with $N_m(t) = 0$.\\

The invasion fitness $s_r(m)$ is given by:
\begin{align*}
s_r(m) =& \beta(m, N^*, 0) - \mu \\
=& F_1(N^*, 0)mA_1(m) + F_2(N^*, 0)(1-m)A_2(1-m)\\
\end{align*}

Note that $s_r(r)=0$ this is an important feature of adaptive dynamics
and checking this is a good test to do when you are trying to apply
it.

Finding $N^*$ is not trivial, we'll do it numerically. Nevertheless we can have a ballpark
estimation by considering that $\delta_1<< A_1(r)N^*$ and $\delta_1<< A_2(r)N^*$:

\begin{align*}
\beta(r, N_r(t), N_m(t)) - \mu &= 0\\
\frac{\delta_1 K_1 A_1(r) r}{A_1(r)rN^*+\delta_1} + \frac{\delta_2 K_2 A_2(r) r}{A_2(r)rN^*+\delta_2} - \mu &= 0\\
\frac{\delta_1 K_1 A_1(r) r}{A_1(r)rN^*} + \frac{\delta_2 K_2 A_2(r) r}{A_2(r)rN^*}  &\approx \mu\\
\frac{\delta_1 K_1 + \delta_2 K_2}{N^*} &\approx  \mu\\\
\frac{\mu}{\delta_1 K_1 + \delta_2 K_2} &\approx  N^*
\end{align*}

* Numerical resolution 
** Clues to get started
Let us write the pseudocode to print the PIP. 

#+BEGIN_SRC python
  def n_star(r, a1, a2, b1, d1, d2, K1, K2, mu):
     """Return the resident population size at equilibrium."""
     # N_r such that of beta(r, N_r) - mu = 0

  def s(r, m, a1, a2, b1, d1, d2, K1, K2, mu):
     """Return the invasion fitness of a mutant m in an environment set by
  the resident r"""
      # beta(m,N^*(r)) - mu 

  def compute_pip(steps, a1, a2, b1, d1, d2, K1, K2, mu):
     """Return a matrix containing the PIP"""
     # Create a matrix stepsXsteps and fill it with s(m,r). 

  def display_pip(pip):
     """Use matplotlib to display the PIP"""
     # display the matrix, in black positive values in white negative values. 

  pip = compute_pip(steps, ...)
  display_pip(pip) 
#+END_SRC

You will need the following functions: =numpy.linspace=, =numpy.zeros=, =scipy.optimize.brentq=,
=matplotlib.pyplot.imshow=. Use the doc to check what they are doing. 

 
** Correction of the code  

#+BEGIN_SRC python
import numpy as np
import scipy.optimize
import matplotlib.pyplot as plt
p = {"a1": 1, "b1": 1, "a2":2, "b2":0, 'd1':1,'d2':1,'K1':1,'K2':1,'mu':0.1}

def n_star(r, a1, a2, b1,b2, d1, d2, K1, K2, mu):
    """Return the resident population size at equilibrium."""
    estimate = (d1*K1+d2*K2)/mu
    A1 = lambda u: a1 + u*b1
    A2 = lambda u: a2 + u*b2
    F1 = lambda N : (d1*K1)/(A1(r)*r*N + d1)
    F2 = lambda N : (d2*K2)/(A2(r)*(1-r)*N + d2)
    beta_minus_mu = lambda N : F1(N) * A1(r) * r + F2(N) * A2(r) * (1-r) - mu
    return scipy.optimize.brentq(beta_minus_mu,0,2*estimate)

def s(m,r,a1,a2,b1,b2,d1,d2,K1,K2,mu):
    """Return the invasion fitness of a mutant m in an environment set by
    the resident r"""
    Nstar = n_star(r, a1, a2, b1,b2, d1, d2, K1, K2, mu)
    f1 = (d1*K1)/((a1+r*b1)*r*Nstar + d1)
    f2 = (d2*K2)/((a2+r*b2)*(1-r)*Nstar + d2)
    return f1*(a1+m*b1)*m + f2*(a2+m*b2)*(1-m) - mu
    
def compute_pip(steps, a1,a2,b1,b2,d1,d2,K1,K2,mu):
    """ Return a the PIP of the model as a matrix """
    pip = np.zeros((steps,steps))
    M = np.linspace(0,1,steps)
    R = np.linspace(0,1,steps)
    for j,r in enumerate(R):
        for i,m in enumerate(M):
            pip[i,j] = s(m,r,a1,a2,b1,b2,d1,d2,K1,K2,mu)
    return pip

def display_pip(pip, ax=None):
    """Use matplotlib to display the PIP"""
    if ax is None:
        ax = plt.gca()
        
    ax.imshow(pip>0,
              cmap='bone_r', 
              origin='bottom', 
              interpolation='none',
              extent=[0,1,1,0])
    ax.set(title='Pairwise invasibility plot', 
           ylabel='Mutant trait m',
           xlabel='resident trait r')
pip = compute_pip(100,**p)
display_pip(pip)
#+END_SRC 


** How to read a pip  

Here are three set of parameters to test: 

#+begin_src python
p_neutral = {"a1": 1, "b1": 0, "a2":1, "b2":0, 'd1':1,'d2':1,'K1':1,'K2':1,'mu':0.1}
p_strong = {"a1": 1, "b1": 1, "a2":2, "b2":0, 'd1':1,'d2':1,'K1':1,'K2':1,'mu':0.1}
p_weak = {"a1": 2, "b1": -1, "a2":1, "b2":0, 'd1':1,'d2':1,'K1':1,'K2':1,'mu':0.1}
#+end_src

What do you conclude for the stability of the evolutionary strategy in
each case ? 

/Let them on their own for a while and then discuss how to read a pip
with the whole group/\\

(maybe draw the four cases on the board ?)

- Convergence stability :: Mutants with a higher (resp lower) value
     -i.e. the ones over (resp. under) the diagonal- can invade the
     resident that have a lower (resp. higher) value than the singular
     strategy (on the left (resp. right) of the singular
     point). /There is a path to get there/. *Look at the line of the
     singular point*.
- Evolutionary stability :: The singular point cannot be invaded by
     any mutant (/Once we are at the singular strategy we stay
     there/.) *look at the column of the singular point*.



* Back to the biology
How could we explain this ? Here is a clue: it is a trade-off problem.
How can we check that mixed strategy fair better or worse than other ?
Plotting $(\text{Search rate}) \times (\text{time spent})$ of one
environment against the other could be a good idea, try it !\\

The stability of this equilibrium depend on the nature of the tradeoff
between the two environments. We distinguish 3 cases inspired by the
work of Levin (Levin 1962, Rueffler 2004):

- Neutral :: if the tradeoff is linear (the search rate only depend
  on the time spent in each environment).
- Strong :: if the tradeoff is convex (i.e. mixed strategies fair
  worse than pure strategies) Then **Selection is disruptive,** we may
  observe an Evolutionary Branching Point.
- Weak :: if the tradeoff is concave (i.e. mixed strategies fair
  better than pure strategies). Then selection **Selection is
  stabilizing,** the population will stay monomorphic.


[[file:pip.pdf]]

[[file:tep.pdf]]




* ee :noexport: 

\frac{dF_1}{dt} &= \delta_1 (K_1 - F_1(t)) - F_1(t)N(t)A_1(u)u  & \text{Density of prey in env. 1}\\
\frac{dF_2}{dt} &=  & \text{Density of prey in env. 1}
\frac{dN}{dt} &=

\begin{equation}
\beta(u) = 
\underbrace{F_1(t)}_{\text{(prey density)}}
\underbrace{A_1(u)}_{\text{(search rate)}}
\underbrace{u}_{\text{(time spent)}}
+ 
\underbrace{F_2(t)}_{\text{(prey density)}}
\underbrace{A_2(u)}_{\text{(search rate)}}
\underbrace{(1-u)}_{\text{(time spent)}}
\end{equation}


For example, in shes, a deep body shape is predicted to be beneﬁcial
in the littoral zone and when preying on sedentary benthic prey, since
it allows a higher precision in manoeuvrability (Webb 1984; Domenici
2003). In contrast, a shallow body shape is predicted to be beneﬁcial
when foraging on planktonic prey (Webb 1984; Domenici 2003).

Interactions between predator- and diet-induced phenotypic changes in
body shape of Crucian carp

In this model (*Claessen, 2007*), a population of fish is spending its time accross two environments, eating two kinds of preys. This population is monomorphic for an adaptive trait $u$ denoting the time spent into one of the two environment. 

The population dynamics is described by an ODE system: 


With the birth rate: 
\begin{equation}
\beta(u) = 
\underbrace{F_1(t)}_{\text{(prey density)}}
\underbrace{A_1(u)}_{\text{(search rate)}}
\underbrace{u}_{\text{(time spent)}}
+ 
\underbrace{F_2(t)}_{\text{(prey density)}}
\underbrace{A_2(u)}_{\text{(search rate)}}
\underbrace{(1-u)}_{\text{(time spent)}}
\end{equation}

The search rates $A_1(u)$ and $A_2(u)$ are considered linear. Empirical evidence shows that $A_2(u)$ is constant (*Andersson, 2003*).  

In this system, Evolutionary singular strategies happen when the ressources are balanced (i.e. $u^*$ such that $F_1(t)A_1(u^*)u^* = F_2(t)A_2(u^*)(1-u^*)$). 

The stability of this equilibrium depend on the nature of the tradeoff between the two environments. We distinguish 3 cases inspired by the work of Levin (Levin 1962, Rueffler 2004):

- **Neutral:** if the tradeoff is linear (the search rate only depend on the time spent in each environment).
- **Strong:** if the tradeoff is convex (i.e. mixed strategies fair worse than pure strategies) Then **Selection is disruptive,** we may observe an Evolutionary Branching Point.
- **Weak:** if the tradeoff is concave (i.e. mixed strategies fair better than pure strategies). Then selection **Selection is stabilizing,** the population will stay monomorphic. 
